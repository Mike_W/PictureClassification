﻿namespace Ucontro
{
    partial class Upanl
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.LBDate = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CBXSale = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CBXType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CBXSoftware = new System.Windows.Forms.ComboBox();
            this.TB_Strory = new System.Windows.Forms.TextBox();
            this.Ucbx_Cancel = new Ucontro.UcheckBox();
            this.Ucbx_open = new Ucontro.UcheckBox();
            this.FlpView = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.AinmTimer = new System.Windows.Forms.Timer(this.components);
            this.flowLayoutPanel1.SuspendLayout();
            this.FlpView.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.LBDate);
            this.flowLayoutPanel1.Controls.Add(this.label2);
            this.flowLayoutPanel1.Controls.Add(this.CBXSale);
            this.flowLayoutPanel1.Controls.Add(this.label3);
            this.flowLayoutPanel1.Controls.Add(this.CBXType);
            this.flowLayoutPanel1.Controls.Add(this.label4);
            this.flowLayoutPanel1.Controls.Add(this.CBXSoftware);
            this.flowLayoutPanel1.Controls.Add(this.TB_Strory);
            this.flowLayoutPanel1.Controls.Add(this.Ucbx_Cancel);
            this.flowLayoutPanel1.Controls.Add(this.Ucbx_open);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(861, 40);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // LBDate
            // 
            this.LBDate.AutoSize = true;
            this.LBDate.Font = new System.Drawing.Font("宋体", 20F);
            this.LBDate.Location = new System.Drawing.Point(3, 0);
            this.LBDate.Name = "LBDate";
            this.LBDate.Size = new System.Drawing.Size(152, 27);
            this.LBDate.TabIndex = 0;
            this.LBDate.Text = "20180909N1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 20F);
            this.label2.Location = new System.Drawing.Point(161, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 27);
            this.label2.TabIndex = 1;
            this.label2.Text = "+";
            // 
            // CBXSale
            // 
            this.CBXSale.Font = new System.Drawing.Font("宋体", 15F);
            this.CBXSale.FormattingEnabled = true;
            this.CBXSale.Location = new System.Drawing.Point(193, 3);
            this.CBXSale.Name = "CBXSale";
            this.CBXSale.Size = new System.Drawing.Size(121, 28);
            this.CBXSale.TabIndex = 2;
            this.CBXSale.Text = "销售";
            this.CBXSale.SelectedValueChanged += new System.EventHandler(this.CBXSale_SelectedValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 20F);
            this.label3.Location = new System.Drawing.Point(320, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 27);
            this.label3.TabIndex = 3;
            this.label3.Text = "+";
            // 
            // CBXType
            // 
            this.CBXType.Font = new System.Drawing.Font("宋体", 15F);
            this.CBXType.FormattingEnabled = true;
            this.CBXType.Location = new System.Drawing.Point(352, 3);
            this.CBXType.Name = "CBXType";
            this.CBXType.Size = new System.Drawing.Size(69, 28);
            this.CBXType.TabIndex = 4;
            this.CBXType.Text = "类型";
            this.CBXType.SelectedValueChanged += new System.EventHandler(this.CBXType_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 20F);
            this.label4.Location = new System.Drawing.Point(427, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 27);
            this.label4.TabIndex = 5;
            this.label4.Text = "+";
            // 
            // CBXSoftware
            // 
            this.CBXSoftware.Font = new System.Drawing.Font("宋体", 15F);
            this.CBXSoftware.FormattingEnabled = true;
            this.CBXSoftware.Location = new System.Drawing.Point(459, 3);
            this.CBXSoftware.Name = "CBXSoftware";
            this.CBXSoftware.Size = new System.Drawing.Size(130, 28);
            this.CBXSoftware.TabIndex = 6;
            this.CBXSoftware.Text = "软件";
            this.CBXSoftware.SelectedValueChanged += new System.EventHandler(this.CBXSoftware_SelectedValueChanged);
            // 
            // TB_Strory
            // 
            this.TB_Strory.Font = new System.Drawing.Font("宋体", 18F);
            this.TB_Strory.Location = new System.Drawing.Point(595, 3);
            this.TB_Strory.Name = "TB_Strory";
            this.TB_Strory.Size = new System.Drawing.Size(149, 35);
            this.TB_Strory.TabIndex = 9;
            this.TB_Strory.Text = "店名";
            this.TB_Strory.TextChanged += new System.EventHandler(this.TB_Strory_TextChanged);
            // 
            // Ucbx_Cancel
            // 
            this.Ucbx_Cancel.AnimSpeed = 0.1F;
            this.Ucbx_Cancel.FrameColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(52)))), ((int)(((byte)(236)))));
            this.Ucbx_Cancel.FrameHeigth = 0;
            this.Ucbx_Cancel.FrameWeight = 3;
            this.Ucbx_Cancel.InnerFrameColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(52)))), ((int)(((byte)(236)))));
            this.Ucbx_Cancel.IsCheck = false;
            this.Ucbx_Cancel.Location = new System.Drawing.Point(750, 3);
            this.Ucbx_Cancel.Name = "Ucbx_Cancel";
            this.Ucbx_Cancel.Size = new System.Drawing.Size(30, 30);
            this.Ucbx_Cancel.TabIndex = 7;
            // 
            // Ucbx_open
            // 
            this.Ucbx_open.AnimSpeed = 0.1F;
            this.Ucbx_open.FrameColor = System.Drawing.Color.Orange;
            this.Ucbx_open.FrameHeigth = 0;
            this.Ucbx_open.FrameWeight = 3;
            this.Ucbx_open.InnerFrameColor = System.Drawing.Color.Orange;
            this.Ucbx_open.IsCheck = false;
            this.Ucbx_open.Location = new System.Drawing.Point(786, 3);
            this.Ucbx_open.Name = "Ucbx_open";
            this.Ucbx_open.Size = new System.Drawing.Size(30, 30);
            this.Ucbx_open.TabIndex = 8;
            this.Ucbx_open.IsCheckEven += new Ucontro.UcheckBox.isCheckEven(this.Ucbx_open_IsCheckEven);
            // 
            // FlpView
            // 
            this.FlpView.AutoSize = true;
            this.FlpView.Controls.Add(this.button1);
            this.FlpView.Dock = System.Windows.Forms.DockStyle.Top;
            this.FlpView.Location = new System.Drawing.Point(0, 40);
            this.FlpView.Name = "FlpView";
            this.FlpView.Size = new System.Drawing.Size(861, 175);
            this.FlpView.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(234, 169);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // AinmTimer
            // 
            this.AinmTimer.Enabled = true;
            this.AinmTimer.Interval = 10;
            this.AinmTimer.Tick += new System.EventHandler(this.AinmTimer_Tick);
            // 
            // Upanl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.FlpView);
            this.Controls.Add(this.flowLayoutPanel1);
            this.DoubleBuffered = true;
            this.Name = "Upanl";
            this.Size = new System.Drawing.Size(861, 286);
            this.Load += new System.EventHandler(this.Upanl_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.FlpView.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label LBDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox CBXSale;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CBXType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CBXSoftware;
        private System.Windows.Forms.FlowLayoutPanel FlpView;
        private System.Windows.Forms.Button button1;
        private UcheckBox Ucbx_open;
        private System.Windows.Forms.Timer AinmTimer;
        public UcheckBox Ucbx_Cancel;
        private System.Windows.Forms.TextBox TB_Strory;
    }
}
