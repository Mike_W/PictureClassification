﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LitJson;
using System.IO;
using Ucontro.Mode;

namespace Ucontro {
    public partial class Upanl : UserControl {
        public struct StcPicCls {
            public List<string> PicGroup;
        };
        public StcPicCls Spc;
        private int minFrameHeight = 40;
        public int MinFrameHeight { get => minFrameHeight; set => minFrameHeight = value; }

      


        private float animSpeed = 0.3f;
        public float AnimSpeed { get => animSpeed; set => animSpeed = value; }


        private string strFileName = "未命名";
        public string StrFileName { get => strFileName; set => strFileName = value; }

        public Upanl() {
            InitializeComponent();
        }
        //动画
        void PainAnim() {
            int w = this.Width;
            int h = this.Height;
            Size tar = new Size(0, 0);
            if (!this.Ucbx_open.IsCheck) {
                h = AnimMathf.Lrep(this.Height, MinFrameHeight + this.FlpView.Height, AnimSpeed);
                tar = new Size(w, h + this.Height);
                if (Math.Abs(this.Height - (MinFrameHeight + this.FlpView.Height)) <= 1) {
                    this.AinmTimer.Enabled = false;
                    return;
                }

            } else {
                h = AnimMathf.Lrep(this.Height, MinFrameHeight, AnimSpeed);
                tar = new Size(w, h + this.Height);
                if (Math.Abs(this.Height - 40) <= 1) {
                    this.AinmTimer.Enabled = false;
                    return;
                    
                }
            }
            this.Size = tar;
        }

        //开始动画
        void StarAnim() {
            this.AinmTimer.Enabled = true;
        }
        //动画帧率
        private void AinmTimer_Tick(object sender, EventArgs e) {
            PainAnim();
        }
        //复选框事件
        private void Ucbx_open_IsCheckEven(EventArgs e) {
            StarAnim();
        }

        private void Upanl_Load(object sender, EventArgs e) {
            InitConfig();

        }
        //初始化配置文件
        public void InitConfig() {
          
            if (!File.Exists(@".\Config.cg")) {

                Information informationMode = new Information();
                informationMode.Sale.AddRange(new List<string>() { "技术", "店面" });
                informationMode.Type.AddRange(new List<string>() { "安装", "维护", "看场地" });
                informationMode.Software.AddRange(new List<string>() { "聚实惠", "来钱快", "天地" });
                string json = JsonMapper.ToJson(informationMode);
                File.WriteAllText(@"./Config.cg", json);
            }else if(File.Exists(@".\Config.cg")) {
                Information HasinformationMode;
                string Hasjson = File.ReadAllText(@".\Config.cg");
                HasinformationMode = JsonMapper.ToObject<Information>(Hasjson);
                this.CBXSale.Items.AddRange(HasinformationMode.Sale.ToArray());
                this.CBXSoftware.Items.AddRange(HasinformationMode.Software.ToArray());
                this.CBXType.Items.AddRange(HasinformationMode.Type.ToArray());
               
            }

            //获取图片

            for (int i = 0; i < Spc.PicGroup.Count; i++) {
                Image img = GetThumbnail(  Image.FromFile(Spc.PicGroup[i]));
                PictureBox pb = new PictureBox();
                pb.Image = img;
                pb.SizeMode = PictureBoxSizeMode.Zoom;
                pb.Size = new Size(200, 200);
                this.FlpView.Controls.Add(pb);
                GC.Collect();
            }

            this.LBDate.Text = StrToLong(Spc.PicGroup[0]);
            GC.Collect();

        }
        string StrToLong(string Instr) {

            string s = Instr;
            long L;
            s = s.Substring(s.Length - 23, 23);
            s = s.Replace("IMG_", "");
            s = s.Replace("_", "");
            s = s.Replace(".jpg", "");
            s = s.Substring(0, 8);
            L = long.Parse(s);
            return L.ToString();
        }
        private void UpdateFileName() {
            StrFileName = this.LBDate.Text +
            this.CBXSale.Text +
            this.CBXType.Text +
            this.CBXSoftware.Text+
            this.TB_Strory.Text;



        }

        private void CBXSale_SelectedValueChanged(object sender, EventArgs e) {
      
            UpdateFileName();
        }

        private void CBXType_SelectedValueChanged(object sender, EventArgs e) {
           
            UpdateFileName();
        }

        private void CBXSoftware_SelectedValueChanged(object sender, EventArgs e) {
           
            UpdateFileName();
        }
        private Image GetThumbnail(Image SorceImg) {
            Image img = new Bitmap(200, 200);
            Graphics g = Graphics.FromImage(img);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
            Rectangle r = new Rectangle(0, 0, 200, 200);
            g.DrawImage(SorceImg, r, 0, 0, SorceImg.Width, SorceImg.Height, GraphicsUnit.Pixel);
            g.Dispose();
            
            return img;
        }

        private void TB_Strory_TextChanged(object sender, EventArgs e) {
            UpdateFileName();
        }
    }
}
