﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Ucontro
{
    public partial class UcheckBox : UserControl
    {

        private bool isCheck = false;
        public bool IsCheck { get => isCheck;
            set { isCheck = value; this.Animtimer.Enabled = true;
                IsCheckEven?.Invoke(new EventArgs());
            }
        }
        private Color frameColor = Color.Black;
        public Color FrameColor { get => frameColor;
        set { frameColor = value; Refresh(); } }
        private Color innerFrameColor = Color.Black;
        public Color InnerFrameColor { get => innerFrameColor;
            set { innerFrameColor = value; }
        }

        private int frameWeight = 3;
        public int FrameWeight { get => frameWeight;
        set { frameWeight = value;Refresh(); } }
        private int frameHeigth = 3;
        public int FrameHeigth { get => frameHeigth;
            set { frameHeigth = value;Refresh(); }
        }
        private float animSpeed = 0.1f;
        public float AnimSpeed { get => animSpeed;
            set { animSpeed = value; }
        }

        public delegate void isCheckEven(EventArgs e);
        public event isCheckEven IsCheckEven;

        Pen p = new Pen(Color.Black, 3);
        Brush b = Brushes.Black;

        public UcheckBox()
        {
            InitializeComponent();
        }

        private void UcheckBox_Paint(object sender, PaintEventArgs e)
        {
            DrawFrame(e.Graphics);
            DrawInnerFrame(e.Graphics);
            
          
           
        }
        //画外框
        void DrawFrame(Graphics g)
        {
            p.Color = FrameColor;
            p.Width = FrameWeight;
            g.DrawRectangle(p, FrameWeight/2, FrameWeight/2, this.Width- FrameWeight, this.Height- FrameWeight);
        }
        //画内框
        void DrawInnerFrame(Graphics g)
        {
            Rectangle r = new Rectangle(0, 0, this.Width, FrameHeigth);
            b = new SolidBrush(InnerFrameColor);
            g.FillRectangle(b, r);
            b.Dispose();
        }
        //动画
        void AnimFrame()
        {

            if (IsCheck)
            {
                FrameHeigth += AnimMathf.Lrep(FrameHeigth, this.Height, AnimSpeed);
                if (Math.Abs(FrameHeigth- this.Height)<1)
                {
                    FrameHeigth = this.Height;
                    this.Animtimer.Enabled = false;
                }
            }
            else
            {

                FrameHeigth += AnimMathf.Lrep(FrameHeigth, 0, AnimSpeed);
                if (Math.Abs(FrameHeigth - 0) < 1)
                {
                    FrameHeigth = 0;
                    this.Animtimer.Enabled = false;
                }
            }
        }

        private void Animtimer_Tick(object sender, EventArgs e)
        {
            AnimFrame();
        }

        private void UcheckBox_Click(object sender, EventArgs e)
        {
            IsCheck = !IsCheck;
        }
    }
}
