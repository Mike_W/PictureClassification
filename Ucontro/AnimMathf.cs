﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ucontro
{
    static class AnimMathf
    {
        static public int Lrep(int Sorce,int Tar,float Speed )
        {
           
                
                int t = Tar - Sorce;
                float r = t * Speed;
                if (Math.Abs(r) < 1)
                {
                    r = (int)Math.Ceiling(r);
                    r = r == 0 ? -1 : r;
                }
                
                return (int)r;
           
            
        }

    }
}
