﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Ucontro;

namespace PictureClassification {
    public partial class MainForm : Form {

        //public struct StcPicCls {
        //    public List<string> PicGroup;
        //};

        private string FolderPath;
        private List<string> PicPatthList = new List<string>();
        private List<string> TempPicPatthList = new List<string>();

        //private List<StcPicCls> StcPicClsList = new List<StcPicCls>();
        private List<Upanl.StcPicCls> StcPicClsList = new List<Upanl.StcPicCls>();
        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {

        }

        private void btnBrowse_Click(object sender, EventArgs e) {

            if (FBD.ShowDialog() == DialogResult.OK) {
                FolderPath = FBD.SelectedPath;
                this.tbFolder.Text = FolderPath;
                Analysis();
            }

        }


        void Analysis() {
            StcPicClsList.Clear();
            TempPicPatthList.Clear();
            PicPatthList.Clear();
            #region  //过滤路径
            if (FolderPath == "" || FolderPath == null) {
                MessageBox.Show("请选择路劲", "提示");
            } else {
                PicPatthList.AddRange(Directory.GetFiles(FolderPath, "*.jpg"));
                while (PicPatthList.Count != 0) {
                    string s = PicPatthList[0];
                    string t = PicPatthList[0];
                    s = s.Substring(s.Length - 23, 4);
                    t = t.Substring(t.Length - 23, 23);
                    t = t.Replace("IMG_", "");
                    t = t.Replace("_", "");
                    t = t.Replace(".jpg", "");

                    if (s == "IMG_" && t.Length == 14) {
                        TempPicPatthList.Add(PicPatthList[0]);

                    }

                    PicPatthList.RemoveAt(0);
                }

                if (TempPicPatthList.Count == 0) {

                    MessageBox.Show("该路径下没有符合要求的JPG文件", "提示");
                    return;
                }
                #endregion
                #region //过滤日期
                for (int i = 0; i < TempPicPatthList.Count; i++) {
                    long L = StrToLong(TempPicPatthList[i]);
                    if (StcPicClsList.Count == 0) {
                        Upanl.StcPicCls tempStcPicCls = new Upanl.StcPicCls { PicGroup = new List<string>() };
                        StcPicClsList.Add(tempStcPicCls);
                        StcPicClsList[0].PicGroup.Add(TempPicPatthList[0]);
                        goto end;
                    }
                    for (int j = 0; j < StcPicClsList.Count; j++) {
                        for (int k = 0; k < StcPicClsList[j].PicGroup.Count; k++) {
                            long ts = StrToLong(StcPicClsList[j].PicGroup[k]);
                            if (Math.Abs(L - ts) < 1000) {
                                StcPicClsList[j].PicGroup.Add(TempPicPatthList[i]);
                                goto end;
                            }
                        }
                        if (j == StcPicClsList.Count - 1) {
                            Upanl.StcPicCls tempStcPicCls = new Upanl.StcPicCls { PicGroup = new List<string>() };
                            StcPicClsList.Add(tempStcPicCls);
                            StcPicClsList[j + 1].PicGroup.Add(TempPicPatthList[i]);
                            goto end;
                        }
                    }






                end:;
                }

            }

            //+++++++++++++++++++++++++++++++++++++++++++++++

            #endregion

            //添加控件

            this.FlpViewMain.Controls.Clear();
            GC.Collect();
            for (int i = 0; i < StcPicClsList.Count; i++) {
                Upanl Tu = new Upanl();
                Tu.Spc = StcPicClsList[i];
                this.FlpViewMain.Controls.Add(Tu);
                GC.Collect();
            }
            InitConform();
            GC.Collect();
        }
        //字符串转数字
        long StrToLong(string Instr) {

            string s = Instr;
            long L;
            s = s.Substring(s.Length - 23, 23);
            s = s.Replace("IMG_", "");
            s = s.Replace("_", "");
            s = s.Replace(".jpg", "");
            L = long.Parse(s);
            return L;
        }
        //设置控件宽度
        void InitConform() {

            for (int i = 0; i < this.FlpViewMain.Controls.Count; i++) {
                //this.FlpViewMain.Controls[i].Height = this.Height;
                this.FlpViewMain.Controls[i].Width = this.Width-100;
            }
        }

        private void MainForm_SizeChanged(object sender, EventArgs e) {
            InitConform();
        }

        private void BtnStar_Click(object sender, EventArgs e) {
            CreatFoled();
        }
        void CreatFoled() {
            Directory.CreateDirectory(FolderPath+@"/OS");
            


            for (int i = 0; i < this.FlpViewMain.Controls.Count; i++) {
                Upanl u = (Upanl)this.FlpViewMain.Controls[i];
                string CopyPath = FolderPath + @"/OS";
                CopyPath += @"/" + u.StrFileName+i;
                if (u.Ucbx_Cancel.IsCheck==true) {
                    goto end;
                }
                Directory.CreateDirectory(CopyPath);
                for (int j = 0; j < u.Spc.PicGroup.Count; j++) {
                 
                    File.Copy(u.Spc.PicGroup[j], CopyPath + @"/"+j + @".jpg",true);

                }
                end:;
            }
            MessageBox.Show("拷贝完成");
        }
    }
}
